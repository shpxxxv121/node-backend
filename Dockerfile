FROM node:5.7.0

run mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . /usr/src/app

EXPOSE 8080

cmd ["npm", "start"]
