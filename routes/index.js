var express = require('express');
var router = express.Router();
var db = require('./db');

router.get('/healtcheck', function (req, res, next) {
    res.send(200);
});

router.post("/api/v1.0/events", function (req, res, next) {
    // Now we happily take this event and just throw it in the db
    var event = req.body;
    var to = event.recipient;

    db.event.create({
        recipient: to,
        blob: JSON.stringify(event)
    });

    return res.send(200);
});

router.get('/api/v1.0/events', function (req, res, next) {
    //?limit=200&after=0&before=234time=1465452384005
    if (req.query.before) {
        // Then we want the first thingos
        return db.event.findAll({
            where: {
                recipient: req.query.recipient,
                id: {
                    $lt: parseInt(req.query.before)
                }
            },
            limit: 200
        }).map(result => {
            var sec = JSON.parse(result.blob);
            sec.id = result.id;
            return sec;
        }).then(sec =>res.send(sec))
    } else if (req.query.after) {

        return db.event.findAll({
            where: {
                recipient: req.query.recipient,
                id: {
                    $gt: parseInt(req.query.after)
                }
            },
            limit: 200
        }).map(result => {
            var sec = JSON.parse(result.blob);
            sec.id = result.id;
            return sec;
        })
            .then(result => res.send(result))
    }

    return db.event.findAll({
        where: {
            recipient: req.query.recipient
        },
        limit: 200
    }).map(result => {
        var sec = JSON.parse(result.blob);
        sec.id = result.id;
        return sec;
    }).then(result => res.send(result))
});

module.exports = router;
