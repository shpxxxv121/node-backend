var Sequelize = require("sequelize")

var sequelize = new Sequelize(process.env['PG_SHIPIT35_EVENTSTREAM_URL'])

var db = {}

var event = sequelize.define('event', {
  recipient: Sequelize.STRING,
  blob: Sequelize.TEXT
});


db.event = event;
db.event.sync();
module.exports = db;
